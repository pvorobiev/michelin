<?
if (!\defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
die();
}
$this->addExternalCss('/local/static/bundle/aboutus.css');
?>

<section class="aboutus">
    <h3 class="main-title">О&nbsp;мишлен</h3>
    <div class="aboutus__description">
        В&nbsp;основе АКСЕЛЕРАТОРА МИШЛЕН&nbsp;&mdash; первый опыт взаимовыгодного партнерства <br>компании Мишлен с&nbsp;Российскими стартапами.
    </div>
    <div class="aboutus__wrapper">
        <div class="raw">
            <div class="cols cols-1">
                <div class="info">
                    <h4>&gt; 50&nbsp;млн&nbsp;&euro;</h4>
                    <p>Компания инвестировала в&nbsp;9&nbsp;венчурных фондов <br>в&nbsp;Европе, Северной Америке и&nbsp;Китае.</p>
                </div>
            </div>
            <div class="cols cols-2">
                <p>Мишлен основана <br>в&nbsp;1889 году</p>
            </div>
            <div class="cols cols-3">
                <p>Широкий спектр шинной продукции</p>
            </div>
        </div>
        <div class="raw">
            <div class="cols cols-4">
                <p>Карты и&nbsp;путеводители ViaMichelin</p>
            </div>
            <div class="cols cols-5">
                <p>68&nbsp;заводов <br>в&nbsp;17&nbsp;странах</p>
            </div>
            <div class="cols cols-6">
                <div class="info">
                    <h4>34&nbsp;стартапа</h4>
                    <p>Дошли до&nbsp;стадии соглашений и&nbsp;работают с&nbsp;Мишлен. Действует <br>активная сеть партнеров среди корпоративных венчурных фондов.</p>
                </div>
            </div>
        </div>
    </div>
</section>