<?
if (!\defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
die();
}
$this->addExternalCss('/local/static/bundle/contacts.css');
?>

<section class="contacts" id="contacts">
    <div class="contacts__main">
        <div class="contacts__map-wrapper">
            <div class="contacts__map">
                <script
                    type="text/javascript"
                    charset="utf-8"
                    async
                    src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A6348ccd2f055cd9786c2b8133461ac5ec5a536e9d4eaf7b598d4cecae0d2e5d8&amp;height=825&amp;lang=ru_RU&amp;scroll=true"></script>
            </div>
        </div>
        
        <div class="contacts__wrapper">
            <h3 class="main-title">Контакты</h3>
            <div class="address">
                <span>Адрес:</span>
                123242, Москва, Малый Конюшковский переулок, д.&nbsp;2
            </div>
            <div class="phone">
                <span>Телефон:</span>
                +7 (495) 777-01-04
            </div>
            <div class="fax">
                <span>Факс:</span>
                +7 (495) 777-01-06
            </div>
            <div class="email">
                <span>E-mail:</span>
                <p><a href="mailto:admin@generation-startup.ru">admin@generation-startup.ru</a>&mdash;&nbsp;техническая поддержка</p>
                <p><a href="mailto:info@generation-startup.ru">info@generation-startup.ru</a>&mdash;&nbsp;организационные вопросы</p>
            </div>
            <div class="socials">
                <span>Социальные сети:</span>
                <div class="icons">
                    <a href="#" target="_blank"><img src="/local/static/assets/img/socials/vkontakte.svg" alt="vk"></a>
                    <a href="#" target="_blank"><img src="/local/static/assets/img/socials/facebook.svg" alt="vk"></a>
                    <a href="#" target="_blank"><img src="/local/static/assets/img/socials/instagram.svg" alt="vk"></a>
                    <a href="#" target="_blank"><img src="/local/static/assets/img/socials/youtube.svg" alt="vk"></a>
                </div>
            </div>
            <div class="contacts__copyright">«Мишлен» не гарантирует предоставление инвестиций и других форм Сотрудничества по совместной программе акселерации, и оставляет за собой право дать отказ на участие в партнерской программе на любом этапе.</div>
        </div>
    </div>
</section>
