<?
if (!\defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
die();
}
$this->addExternalCss('/local/static/bundle/demands.css');
?>

<section class="demands">
    <h3 class="main-title">Требования к&nbsp;проектам</h3>
    <div class="demands__wrapper">
        <div class="demands__wrapper__item">
            <img src="/local/static/assets/img/icons-demand/1.svg" alt="icon">
            <div class="description">
                <h4>Стадия проекта:</h4>
                <span>Прототип и&nbsp;выше</span>
            </div>
        </div>
        <div class="demands__wrapper__item">
            <img src="/local/static/assets/img/icons-demand/2.svg" alt="icon">
            <div class="description">
                <h4>Желательный уровень <br> владения английским языком:</h4>
                <span>Intermediate /<br>Upper&nbsp;&mdash; Intermediate</span>
            </div>
        </div>
        <div class="demands__wrapper__item">
            <img src="/local/static/assets/img/icons-demand/3.svg" alt="icon">
            <div class="description">
                <h4>Наличие юридического лица:</h4>
                <span>Зарегистрированное юридическое лицо/<br>В&nbsp;процессе регистрации <br>в&nbsp;ближайшие три месяца</span>
            </div>
        </div>
    </div>
</section>