<?
if (!\defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
die();
}
$this->addExternalCss('/local/static/bundle/directions.css');
?>

<section class="directions" id="directions">
    <div class="directions__wrapper">
        <div class="title-wrapper">
            <h3 class="main-title">Направления акселератора</h3>
            <p class="directions__description">Мы&nbsp;ищем технологии, которые изменят жизнь водителей <br> грузового транспорта на&nbsp;дороге!</p>
        </div>
        <div class="directions__card-wrapper">
            <div class="directions__card">
                <img src="/local/static/assets/img/icons-directions/1.svg" alt="icon">
                <h4><strong>Персональная медицина</strong></h4>
                <ul>
                    <li>диагностика, телемедицина во&nbsp;время движения;</li>
                    <li>помощь во&nbsp;время аварий на&nbsp;дорогах, определение риска дорожно-транспортных происшествий;</li>
                    <li>предупреждающие сигналы на&nbsp;дороге;</li>
                    <li>физический комфорт водителя.</li>
                </ul>
            </div>
    
            <div class="directions__card">
                <img src="/local/static/assets/img/icons-directions/2.svg" alt="icon">
                <h4><strong>Безопасность во&nbsp;время остановок</strong> <br>(для грузовика, водителя и&nbsp;груза)</h4>
                <ul>
                    <li>безопасность на&nbsp;дороге (особенно во&nbsp;время отдыха водителя);</li>
                    <li>технологии для комфортного ремонта;</li>
                    <li>предупреждение проблем с&nbsp;дорожным полотном.</li>
                </ul>
            </div>
    
            <div class="directions__card">
                <img src="/local/static/assets/img/icons-directions/3.svg" alt="icon">
                <h4><strong>Безопасное вождение</strong></h4>
                <ul>
                    <li>своевременное определение опасности;</li>
                    <li>определение дорожных рисков;</li>
                    <li>места со&nbsp;сложной дорожной обстановкой;</li>
                    <li>взаимодействие с&nbsp;компьютером в&nbsp;чрезвычайных ситуациях;</li>
                    <li>взаимодействие с&nbsp;веб-интерфейсом (компьютером) за&nbsp;рулем;</li>
                    <li>отображение опасных ситуаций (пешеход на&nbsp;дороге, яма) на&nbsp;лобовом стекле.</li>
                </ul>
            </div>
    
            <div class="directions__card">
                <img src="/local/static/assets/img/icons-directions/4.svg" alt="icon">
                <h4><strong>Социальная жизнь<br>и&nbsp;коммуникации</strong> (между водителями в&nbsp;удаленных районах)</h4>
                <ul>
                    <li>коммуникационные протоколы (когда другая связь недоступна);</li>
                    <li>сообщество водителей грузовиков.</li>
                </ul>
            </div>
    
            <div class="directions__card">
                <img src="/local/static/assets/img/icons-directions/5.svg" alt="icon">
                <h4><strong>Услуги для малых предприятий</strong><br>(когда водитель сам владеет <br> транспортной/логистической компанией)</h4>
                <ul>
                    <li>технологии для водителей, управляющих бизнесом&nbsp;&mdash; безопасный и&nbsp;комфортный способ решения бизнес-задач в&nbsp;дороге.</li>
                </ul>
            </div>
        </div>
    </div>
</section>