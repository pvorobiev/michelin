<?
if (!\defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
die();
}
$this->addExternalCss('/local/static/bundle/experts.css');
?>

<section class="experts">
    <h3 class="main-title">Эксперты</h3>
    <div class="experts__wrapper">
        
        <div class="experts__wrapper__card">
            <img src="/local/static/assets/img/experts/1.png" alt="expert">
            <h4 class="experts-title">
                Петрова Екатерина
            </h4>
            <div class="experts-description">
                Руководитель акселератора GenerationS
            </div>
        </div>
        
        <div class="experts__wrapper__card">
            <img src="/local/static/assets/img/experts/2.png" alt="expert">
            <h4 class="experts-title">
                Госелин Ксавье
            </h4>
            <div class="experts-description">
                Менеджер проекта
            </div>
        </div>

        <div class="experts__wrapper__card">
            <img src="/local/static/assets/img/experts/3.png" alt="expert">
            <h4 class="experts-title">
                Фури Фредерик
            </h4>
            <div class="experts-description">
                Заместитель директора европейского<br>бизнес-инкубатора группы Мишлен
            </div>
        </div>

        <div class="experts__wrapper__card">
            <img src="/local/static/assets/img/experts/4.png" alt="expert">
            <h4 class="experts-title">
                Жанн Пьер-Ив
            </h4>
            <div class="experts-description">
                Коммерческий директор Департамента <br>интернет продаж, сервисных решений и <br>бизнес услуг Восточно-Европейского региона
            </div>
        </div>

        <div class="experts__wrapper__card">
            <img src="/local/static/assets/img/experts/5.png" alt="expert">
            <h4 class="experts-title">
                Шамсуддинов Рамиль
            </h4>
            <div class="experts-description">
                Директор по&nbsp;продажам сервисов и&nbsp;решений
            </div>
        </div>

        <div class="experts__wrapper__card">
            <img src="/local/static/assets/img/experts/6.png" alt="expert">
            <h4 class="experts-title">
                Файзулина Виктория
            </h4>
            <div class="experts-description">
                Менеджер по&nbsp;развитию взаимоотношений <br>с&nbsp;потребителями B2B
            </div>
        </div>

    </div>
</section>