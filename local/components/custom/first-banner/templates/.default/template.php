<?
if (!\defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
die();
}
$this->addExternalCss('/local/static/bundle/first-banner.css');
?>

<section class="first-banner">
    <div class="first-banner__wrapper">
        <div class="first-banner__grid">
            <div class="left-block">
                <h1>Акселератор <br>мишлен</h1>
                <span class="green-line"></span>
                <div class="first-banner__description">
                    Станьте частью Акселератора компании Мишлен<br>и&nbsp;помогите водителям получить опыт безопасного, комфортного и&nbsp;&laquo;умного&raquo; вождения!
                </div>
                <a href="#" class="button--send">
                    <div class="first-banner__send-button">
                        Подать заявку
                    </div>
                </a>
                <div class="first-banner__description">
                    <strong>АКСЕЛЕРАТОР МИШЛЕН</strong>&nbsp;&mdash; акселератор для стартапов в&nbsp;сфере модернизации транспортных средств, решений для водителей и&nbsp;пассажиров большегрузных автомобилей.
                </div>
            </div>
        </div>
    </div>
    <div class="bitmap-image">
        <img src="/local/static/assets/img/truck/truck.png" class="Truck" alt="truck">
    </div>
    
    
</section>