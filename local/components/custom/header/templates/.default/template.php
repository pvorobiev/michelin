<?
if (!\defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
die();
}
$this->addExternalCss('/local/static/bundle/header.css');
?>

<div class="header-menu">
    <div class="header-menu__inner">
        <div class="logo"><img src="/local/static/assets/img/group-40.svg" alt="Логотип Michelin"></div>
        <div class="logo-gens"><img src="/local/static/assets/img/gen-s-2.svg" alt="Лого Gen-S"></div>
        <nav class="menu">
            <a class="menu-item" href="#section-prize">Об акселераторе</a>
            <a class="menu-item" href="#directions">Направления</a>
            <a class="menu-item" href="#timeline">График</a>
            <a class="menu-item" href="#posibilities">Преимущества</a>
            <a class="menu-item" href="#contacts">Контакты</a>
        </nav>
        <a href="#"><div class="button-send">Подать заявку</div></a>
        <a href="#" class="eng">En</a>
        <div class="burger"></div>
    </div>
    <nav class="burger-menu">
        <ul class="burger-menu__links">
            <a href="#"><li class="lead">Подать заявку</li></a>
            <a href="#section-prize"><li>Об акселераторе</li></a>
            <a href="#directions"><li>Направления</li></a>
            <a href="#timeline"><li>График</li></a>
            <a href="#posibilities"><li>Преимущества</li></a>
            <a href="#contacts"><li>Контакты</li></a>
            <li class="burger-social">
                <a href="#" target="_blank"><img src="/local/static/assets/img/socials/vkontakte-white.svg" alt="vk"></a>
                <a href="#" target="_blank"><img src="/local/static/assets/img/socials/facebook-white.svg" alt="fb"></a>
                <a href="#" target="_blank"><img src="/local/static/assets/img/socials/instagram-white.svg" alt="instagram"></a>
                <a href="#" target="_blank"><img src="/local/static/assets/img/socials/youtube-white.svg" alt="youtube"></a>
            </li>
            <a href="#" class="eng-version"><li>English version</li></a>
        </ul>
    </nav>
</div>
