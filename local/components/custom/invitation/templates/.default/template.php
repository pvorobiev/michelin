<?
if (!\defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
die();
}
$this->addExternalCss('/local/static/bundle/invitation.css');
?>

<section class="section-invite">
    <div class="section-invite__wrapper">
        <h3>К&nbsp;участию в&nbsp;акселераторе приглашаются стартап-команды, которые могут помочь улучшить жизнь водителей на&nbsp;дорогах.</h3>
        <p>В&nbsp;ходе акселерации, команды смогут детально познакомиться с&nbsp;производственным процессом Мишлен и&nbsp;разработать совместный план по&nbsp;интеграции.</p>
        <a href="#" class="button--send">
            <div class="section-invite__send-button">
                Подать заявку
            </div>
        </a>
    </div>
</section>