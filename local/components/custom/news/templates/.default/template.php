<?
if (!\defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
die();
}
$this->addExternalCss('/local/static/bundle/news.css');
?>

<section class="news">
    <h3 class="main-title">Новости и&nbsp;мероприятия</h3>
    <div class="news__wrapper">
        <a href="#" data-toggle="modal" data-target="#ModalCenter">
            <div class="news__wrapper__card">
                <img src="/local/static/assets/img/news/news1.png" alt="news-item">
                <h4 class="news-title">
                    24&nbsp;мая 2018
                </h4>
                <div class="news-description">
                    ВТБ и&nbsp;GenerationS запускают корпоративный <br>финтех-акселератор
                </div>
                
            </div>
        </a>
        <a href="#" data-toggle="modal" data-target="#ModalCenter">
            <div class="news__wrapper__card">
                <img src="/local/static/assets/img/news/news2.png" alt="news-item">
                <h4 class="news-title">
                    28&nbsp;апреля 2018
                </h4>
                <div class="news-description">
                    Названы лучшие технологические стартапы <br>акселератора GenerationS
                </div>
            </div>
        </a>
        <a href="#" data-toggle="modal" data-target="#ModalCenter">
            <div class="news__wrapper__card">
                <img src="/local/static/assets/img/news/news3.png" alt="news-item">
                <h4 class="news-title">
                    12&nbsp;марта 2018
                </h4>
                <div class="news-description">
                    Три финалиста GenerationS получат 6&nbsp;млн рублей<br>от&nbsp;Фонда содействия инновациям
                </div>
            </div>
        </a>
        <a href="#" data-toggle="modal" data-target="#ModalCenter">
            <div class="news__wrapper__card">
                <img src="/local/static/assets/img/news/news2.png" alt="news-item">
                <h4 class="news-title">
                    28&nbsp;апреля 2018
                </h4>
                <div class="news-description">
                    Названы лучшие технологические стартапы <br>акселератора GenerationS
                </div>
            </div>
        </a>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">ВТБ и&nbsp;GenerationS запускают корпоративный финтех-акселератор</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" data-simplebar="init" data-simplebar-auto-hide="false">
                    <div class="modal-info">
                        <div class="description-0">
                            <div class="info-item"><img src="/local/static/assets/img/modal/date.svg" alt="date" class="date">10&nbsp;июля 2018</div>
                            <div class="info-item"><img src="/local/static/assets/img/modal/time.svg" alt="time" class="time">17:30&nbsp;&mdash; 19:30</div>
                            <div class="info-item"><img src="/local/static/assets/img/modal/location.svg" alt="location" class="location">г. Москва, Малый Конюшковский переулок, д.2, 3&nbsp;этаж (&laquo;Точка кипения&raquo;, 2&nbsp;этаж)</div>
                        </div>
                        
                        <div class="description-1">
                            Группа ВТБ запускает корпоративный финтех-акселератор на&nbsp;базе GenerationS, крупнейшего акселератора стартапов России и&nbsp;Восточной Европы от&nbsp;РВК. Корпоративный акселератор ВТБ направлен на&nbsp;поиск и&nbsp;развитие технологических проектов, чьи решения могут быть внедрены в&nbsp;компаниях группы ВТБ.
                        </div>
                        
                        <div class="description-2">
                            <img src="/local/static/assets/img/news/news1.png" alt="modal-news">
                            <p>В&nbsp;рамках XXII Петербургского международного экономического форума соглашение о&nbsp;создании акселератора подписали заместитель президента-председателя правления банка ВТБ Ольга Дергунова и&nbsp;генеральный директор РВК Александр Повалко.
                            </p>
                            <p>Соглашение дает старт сбору заявок от&nbsp;стартапов на&nbsp;участие в&nbsp;финтех-акселераторе ВТБ. Подать заявки смогут команды, разрабатывающие решения для финансового и&nbsp;околофинансового сектора в&nbsp;области больших данных, искусственного интеллекта, биометрии, блокчейна, интернета вещей, цифровизации бизнес-процессов и&nbsp;др.
                               Эксперты банка и&nbsp;GenerationS совместно оценят заявки, отберут около 40&nbsp;наиболее перспективных команд и&nbsp;помогут командам доработать технологические решения для последующей интеграции в&nbsp;бизнес-процессы компаний группы ВТБ. Во&nbsp;время акселерационной программы с&nbsp;проектами будут работать специалисты по&nbsp;ИТ, финансам, маркетингу, продажам и&nbsp;патентному сопровождению.
                               В&nbsp;марте 2019 года ВТБ объявит победителей&nbsp;&mdash; стартапы, которые будут выбраны для проведения пилотных проектов в&nbsp;банке. Успешные проекты смогут претендовать на&nbsp;долгосрочное партнерство с&nbsp;ВТБ и&nbsp;инвестиции.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>