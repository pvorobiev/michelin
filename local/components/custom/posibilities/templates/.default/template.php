<?
if (!\defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
die();
}
$this->addExternalCss('/local/static/bundle/posibilities.css');
?>

<section class="posibilities" id="posibilities">
    <h3 class="main-title">Какие возможности дает акселератор?</h3>
    <div class="posibilities__wrapper">
        <div class="posibilities__wrapper__card">
            <img src="/local/static/assets/img/icons-possib/1.svg" alt="icon">
            <div class="description">
                Сотрудничество с&nbsp;лидером отрасли
            </div>
        </div>

        <div class="posibilities__wrapper__card">
            <img src="/local/static/assets/img/icons-possib/2.svg" alt="icon">
            <div class="description">
                Разработка бизнес-плана проекта <br>с&nbsp;российскими и&nbsp;зарубежными экспертами
            </div>
        </div>

        <div class="posibilities__wrapper__card">
            <img src="/local/static/assets/img/icons-possib/3.svg" alt="icon">
            <div class="description">
                Доступ к&nbsp;инфраструктуре <br>GenerationS
            </div>
        </div>

        <div class="posibilities__wrapper__card">
            <img src="/local/static/assets/img/icons-possib/4.svg" alt="icon">
            <div class="description">
                <p>Шанс привлечь инвестиции <br>от&nbsp;венчурного фонда МИШЛЕН</p>
                <p>Шанс получить 20&nbsp;000&nbsp;&euro; на&nbsp;развитие проекта</p>
            </div>
        </div>
    </div>
</section>