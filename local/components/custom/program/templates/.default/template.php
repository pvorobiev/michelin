<?
if (!\defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
die();
}
$this->addExternalCss('/local/static/bundle/program.css');
?>

<section class="program">
    <h3 class="main-title">Что входит в&nbsp;программу?</h3>
    
    <div class="program__wrapper">
        <div class="program__wrapper__item">
            <span class="ribbon">
                <span>3-х недельная программа</span>
            </span>
            <p>Интенсивная 3-х недельная программа, направленная на&nbsp;развитие технического<br>и&nbsp;бизнес потенциала вашего проекта.</p>
        </div>
        <div class="program__wrapper__item">
            <span class="ribbon">
                <span>Экспертная оценка</span>
            </span>
            <p>Получите экспертную оценку вашего проекта.</p>
        </div>
        <div class="program__wrapper__item">
            <span class="ribbon">
                <span>Мастер-классы и&nbsp;воркшопы</span>
            </span>
            <p>Программа будет разработана совместно с&nbsp;представителями Мишлен и&nbsp;отвечает бизнес запросам заказчика.</p>
        </div>
        <div class="program__wrapper__item">
            <span class="ribbon">
                <span>Ментор для каждой команды</span>
            </span>
            <p>В&nbsp;течение всей акселерационной программы<br>с&nbsp;вами будет работать один из&nbsp;наших менторов.</p>
        </div>
        <div class="program__wrapper__item">
            <span class="ribbon">
                <span>DEMO DAY</span>
            </span>
            <p>Лучшие проекты представляют свои результаты перед инвесторами и&nbsp;потенциальными<br>партнёрами на&nbsp;финальном DEMO DAY.</p>
        </div>
    </div>
</section>