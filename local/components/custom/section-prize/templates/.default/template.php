<?
if (!\defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
die();
}
$this->addExternalCss('/local/static/bundle/section-prize.css');
?>

<section class="section-prize" id="section-prize">
    <div class="section-prize__wrapper">
        <div class="section-prize__img">
            <img src="/local/static/assets/img/marshmallow.svg" alt="michelin">
        </div>
        <div class="section-prize__description">
            <h3>Главный приз</h3>
            <h2 class="contract">Заключение договора о&nbsp;сотрудничестве</h2>
            <h2 class="money">20&nbsp;000&nbsp;&euro;</h2>
            <p>и&nbsp;поездка в&nbsp;головной офис одного <br>из&nbsp;лидирующих производителей <br> автомобильных шин&nbsp;&mdash; <strong>МИШЛЕН</strong> во&nbsp;Франции</p>
            <p class="mobile">и&nbsp;предложение о&nbsp;сотрудничестве от&nbsp;одного из&nbsp;лидирующих французских производителей автомобильных шин&nbsp;&mdash; <strong>МИШЛЕН</strong></p>
        </div>
    </div>
</section>