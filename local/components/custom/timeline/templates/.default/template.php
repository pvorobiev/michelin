<?
if (!\defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
die();
}
$this->addExternalCss('/local/static/bundle/timeline.css');
?>

<section class="timeline" id="timeline">
    <h3 class="main-title">График мероприятий</h3>
    <div class="timeline__wrapper">
        <ul>
            <li>
                <div class="status status-1">
                    <span class="point active-point"></span>
                    <div class="description">
                        <h4>1&nbsp;апреля&nbsp;&mdash; 31&nbsp;июля</h4>
                        <span>сбор заявок</span>
                    </div>
                </div>
            </li>
            <li>
                <div class="status status-2">
                    <span class="point"></span>
                    <div class="description">
                        <h4>1&ndash;31 августа</h4>
                        <span>экспертиза</span>
                    </div>
                </div>
            </li>
            <li>
                <div class="status status-3">
                    <span class="point"></span>
                    <div class="description">
                        <h4>1&ndash;30 сентября</h4>
                        <span>акселерация</span>
                    </div>
                </div>
            </li>
            <li>
                <div class="status status-4">
                    <span class="point"></span>
                    <div class="description">
                        <h4>Октябрь</h4>
                        <span>финал</span>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <a href="#" class="button--send">
        <div class="timeline__send-button">
            Подать заявку
        </div>
    </a>
    
</section>