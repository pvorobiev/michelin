<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
<script
    src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
    crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js"></script>
<script type="text/javascript">
    'use strict';
    $(document).ready(function(){
        var toggle = 0;
        $(".menu").on("click","a", function (event) {
            event.preventDefault();
            var id  = $(this).attr('href'),
            top = $(id).offset().top;
            $('body,html').animate({scrollTop: top}, 1500);
        });

        $(".burger-menu__links").on("click","a", function (event) {
            event.preventDefault();
            
            var id  = $(this).attr('href'),
                top = $(id).offset().top;
            $('body,html').animate({scrollTop: top}, 1500);

            toggle = 0;
            TweenMax.to(".burger-menu__links", 1, {x:1000});
            $('.header-menu').css('background-color', 'white');
            $('.logo > img').attr('src', '/local/static/assets/img/group-40.svg');
            $('.logo-gens > img').attr('src', '/local/static/assets/img/gen-s-2.svg');
            $('.burger').css('background', 'url(/local/static/assets/img/group.svg) no-repeat center');
        });

        
        
        $('.burger').click(function() {
            if (toggle == 0){
                toggle = 1;
                $('.burger-menu').addClass('burger-active');
                $('.burger-menu').css('display', 'flex');
                TweenMax.to(".burger-menu__links", 0.5, {x:0.1});
                $('.header-menu').css('background-color', '#004f9e');
                $('.logo > img').attr('src', '/local/static/assets/img/group-40-copy.svg');
                $('.logo-gens > img').attr('src', '/local/static/assets/img/gen-s-2-copy.svg');
                $('.burger').css('background', 'url(/local/static/assets/img/cancel.svg) no-repeat center');
                $('.burger').css('background-size', 'cover');
                $('.burger').css('width', '15px');
                $('.burger').css('height', '15px');
                $('.burger').css('right', '20px');
            }
            else {
                toggle = 0;
                TweenMax.to(".burger-menu__links", 1, {x:1000});
                $('.header-menu').css('background-color', 'white');
                $('.logo > img').attr('src', '/local/static/assets/img/group-40.svg');
                $('.logo-gens > img').attr('src', '/local/static/assets/img/gen-s-2.svg');
                $('.burger').css('background', 'url(/local/static/assets/img/group.svg) no-repeat center');
            }
        });
        
        
        $('.news__wrapper').slick({
            infinite: true,
            speed: 300,
            slidesToShow: 3,
            adaptiveHeight: true,
            nextArrow: '<span class="arrow-right"></span>',
            prevArrow: '<span class="arrow-left"></span>',
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 769,
                    settings: {
                        centerMode: true,
                        centerPadding: '10px',
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 569,
                    settings: {
                        centerMode: false,
                        slidesToShow: 1
                    }
                }
            ]
        });
        
        var el = document.querySelector('.modal-body');
        SimpleScrollbar.initEl(el);


        $(window).on('resize', function (event) {
            if ($(window).width() > 950) {
                toggle = 0;
                TweenMax.to(".burger-menu__links", 1, {x:1000});
                $('.header-menu').css('background-color', 'white');
                $('.logo > img').attr('src', '/local/static/assets/img/group-40.svg');
                $('.logo-gens > img').attr('src', '/local/static/assets/img/gen-s-2.svg');
                $('.burger').css('background', 'url(/local/static/assets/img/group.svg) no-repeat center');
                $('.burger-menu').css('display', 'none');
            }
        });
    });

    
</script>
</body>
</html>
