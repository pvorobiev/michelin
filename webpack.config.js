require('webpack');
let path = require('path');

const ExtractTextPlugin = require("extract-text-webpack-plugin");
const autoprefixer = require('autoprefixer');
const extractSass = new ExtractTextPlugin({
    filename: "[name].css",
    allChunks: true,
    disable: false
});

// Устанавливаем дополнительные пути статики
// =========================================
const nodePath            = path.join('node_modules/');
const customNodePath      = path.join('local/static/modules/');
const customNodePathBuild = path.join('local/static/build/');
module.paths.unshift(customNodePath);
module.paths.unshift(customNodePathBuild);



// Стартовый конфиг
// ===============
let config = {
    entry:  {
        // пример entry - меняем их на свои
        'project': './local/static/assets/sass/project.sass',
        'header': './local/components/custom/header/templates/.default/index.sass',
        'first-banner': './local/components/custom/first-banner/templates/.default/index.sass',
        'section-prize': './local/components/custom/section-prize/templates/.default/index.sass',
        'invitation': './local/components/custom/invitation/templates/.default/index.sass',
        'directions': './local/components/custom/directions/templates/.default/index.sass',
        'demands': './local/components/custom/demands/templates/.default/index.sass',
        'timeline': './local/components/custom/timeline/templates/.default/index.sass',
        'program': './local/components/custom/program/templates/.default/index.sass',
        'news': './local/components/custom/news/templates/.default/index.sass',
        'experts': './local/components/custom/experts/templates/.default/index.sass',
        'posibilities': './local/components/custom/posibilities/templates/.default/index.sass',
        'aboutus': './local/components/custom/aboutus/templates/.default/index.sass',
        'contacts': './local/components/custom/contacts/templates/.default/index.sass',
    },
    output: {
        path:       path.resolve(__dirname, 'local/static/bundle'),
        filename:   '[name].css',
        publicPath: '/local/static/bundle/'
    },
    module: {
        rules: [

            // JSX
            // ===
            {
                test:    /\.jsx$/,
                exclude: /node_modules/,
                loader:  'babel-loader'
            },

            // CSS
            // ===
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: 'css-loader'
                })
            },

            // SASS
            // ====
            {
                test: /\.(scss|sass)$/,
                use: extractSass.extract({
                    fallback: 'style-loader',
                    use: [
                            {
                                loader: 'raw-loader'
                            },
                            {
                                loader: 'postcss-loader',
                                options: {
                                    plugins: [
                                        autoprefixer({
                                            browsers:['ie >= 8', 'last 25 version']
                                        }),
                                        require('cssnano')({
                                            preset: 'default',
                                        })
                                    ],
                                    sourceMap: true
                                }
                            },
                            {
                                loader: 'sass-loader'
                            }
                        ],

                })
            }
        ]
    },

    plugins: [
        extractSass
    ],
    watch: true,
    // игнорируем папку с модулями для скорости. Можно раскомментировать.
    watchOptions: {ignored: /node_modules/},

    // метод сборки source-map. Для скорости можно менять.
    //devtool: "cheap-eval-source-map",
    devtool: "source-map",

    resolve:       {
        modules:    [nodePath, customNodePath, customNodePathBuild],
        extensions: ['.js', '.json', '.jsx']
    },
    resolveLoader: {
        modules: [nodePath]
    }
};

module.exports = config;
